import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './pages/profile/profile.component';
import { LayoutComponent } from './pages/layout/layout.component';
import { NavbarComponent } from './pages/layout/navbar/navbar.component';
import { AddComponent } from './pages/profile/add/add.component';
import { EditComponent } from './pages/profile/edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    LayoutComponent,
    NavbarComponent,
    AddComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
