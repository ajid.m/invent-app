import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  constructor(private _http: HttpClient) {}

  districts(cityId) {
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'get/districts?city_id=' + cityId;
    return this._http.get(url, { headers: httpHeaders });
  }

  cities(provinceId) {
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url =
      environment.url + 'get/citys?province_id=' + provinceId;
    return this._http.get(url, { headers: httpHeaders });
  }

  provinces() {
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'get/province';
    return this._http.get(url, { headers: httpHeaders });
  }
}
