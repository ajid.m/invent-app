import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  constructor(private _http: HttpClient) {}

  get() {
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'get/ajid_2dec11-30';
    return this._http.get(url, { headers: httpHeaders });
  }

  find() {
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = 'session/login';
    return this._http.get(url, { headers: httpHeaders });
  }

  create(dataValue) {
    const params = {
      postDta: {
        tbl: environment.tableName,
        data: dataValue
      }
    };
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'post';
    return this._http.post(url, JSON.stringify(params), {
      headers: httpHeaders
    });
  }

  update(queryValue, args) {
    const params = {
      postDta: {
        tbl: environment.tableName,
        query: queryValue,
        set: args
      }
    };
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'update';
    return this._http.post(url, JSON.stringify(params), {
      headers: httpHeaders
    });
  }

  delete(queryValue) {
    const params = {
      postDta: {
        tbl: environment.tableName,
        query: queryValue
      }
    };
    const httpHeaders = new HttpHeaders().set(
      'Content-Type',
      'application/json'
    );
    const url = environment.url + 'delete';
    return this._http.post(url, JSON.stringify(params), {
      headers: httpHeaders
    });
  }
}
