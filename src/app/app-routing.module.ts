import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Page Components
import { ProfileComponent } from './pages/profile/profile.component';
import { AddComponent } from './pages/profile/add/add.component';
import { EditComponent } from './pages/profile/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    data: {
      override: true,
      state: 'profile'
    }
  },
  {
    path: '/add',
    component: AddComponent,
    data: {
      override: true,
      state: 'add'
    }
  },
  {
    path: '/edit',
    component: EditComponent,
    data: {
      override: true,
      state: 'edit'
    }
  },
  ,
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
