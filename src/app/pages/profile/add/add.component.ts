import { Component, OnInit } from '@angular/core';
import { GeneralService } from './../../../services/general.service';
import { ProfileService } from './../../../services/profile.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  userProfile: any;
  province: any;
  cities: any;
  districts: any;
  constructor(
    public _profileServices: ProfileService,
    public _generalService: GeneralService
  ) {}

  ngOnInit() {}

  getProvince() {
    this.province = this._generalService.provinces();
  }

  getCity(provinceId) {
    this.cities = this._generalService.cities(provinceId);
  }

  getDistricts(cityId) {
    this.districts = this._generalService.districts(cityId);
  }

  findUser() {
    this.userProfile = this._profileServices.find();
  }

  updateUser() {
    const params = {};
    this.userProfile = this._profileServices.create(params);
  }
}
