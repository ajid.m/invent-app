import { Component, OnInit } from '@angular/core';
import { GeneralService } from './../../../services/general.service';
import { ProfileService } from './../../../services/profile.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  userProfile: any;
  province: any;
  cities: any;
  districts: any;
  constructor(
    public _profileServices: ProfileService,
    public _generalService: GeneralService
  ) {}

  ngOnInit() {}

  getProvince() {
    this.province = this._generalService.provinces();
  }

  getCity(provinceId) {
    this.cities = this._generalService.cities(provinceId);
  }

  getDistricts(cityId) {
    this.districts = this._generalService.districts(cityId);
  }

  findUser() {
    this.userProfile = this._profileServices.find();
  }

  updateUser() {
    const userId = '';
    const params = {};
    this.userProfile = this._profileServices.update(userId, params);
  }
}
