import { Component, OnInit } from '@angular/core';
import { ProfileService } from './../../services/profile.service';
import { GeneralService } from './../../services/general.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userProfile: any;
  constructor(
    public _profileServices: ProfileService,
    public _generalService: GeneralService
  ) {}

  ngOnInit() {}

  getData() {
    this.userProfile = this._profileServices.get();
  }

  finduUser() {
    this.userProfile = this._profileServices.find();
  }
}
